# Generate invoices

Quick-and-dirty script to import a CSV of business data and generate a PDF
invoice for each client.

Uses LaTeX under the hood.

As part of the quick-and-dirtiness, the format of the CSV is undocumented --
look at the code, and if the code is not comprehensible, then ope.
