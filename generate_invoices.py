from datetime import datetime
import pandas as pd
from pathlib import Path
import subprocess

input_file = Path("Payments-Grid view.csv")
output_dir = Path("latex_files")
# Create output dir if it doesn't already exist
output_dir.mkdir(parents=True, exist_ok=True)

# Note: the things inside {braces} are placeholders recognized by
# `str.format(...)` which we call below.
#
# If you want literal braces (which in some places we do), use {{double
# braces}} -- they will come out as single braces on the other end.
LATEX_TEMPLATE = r"""
\documentclass[11pt]{{letter}}

\begin{{document}}

Dear {contact_name},

Thank you very much for your business.
We have in our records that your organization, {org}, used our services on {date}, for a fee of {fee}.
{whether_paid}

Thanks! \\
Julian

\end{{document}}
"""

def latex_escape(s):
    """Crude function to escape LaTeX special characters."""
    if isinstance(s, str):
        return s.replace("$", r"\$")
    return s

def as_filename(s):
    """Returns a string similar to `s` that is suitable for use as a filename.

    E.g., the returned string won't contain slashes, even if `s` does."""
    return s.replace("/", "_")

print("Reading the data...")
df = pd.read_csv(input_file)
print("Done reading the data.")

print("Generating LaTeX files...")
latex_file_paths = []
for _, row in df.iterrows():
    latex_src = LATEX_TEMPLATE.format(
                    contact_name = row["Contact Name"],
                    org = row["Organization"],
                    fee = latex_escape(row["Fee"]),
                    date = row["Screening Date"],
                    whether_paid = ("The fee has already been paid and you have no remaining balance.  Thank you!"
                                    if row["Paid"] == "checked"
                                    else "We have not yet received payment and request you please contact us to settle your remaining balance."),
                    )
    outpath = output_dir / f"{as_filename(row['Organization'])}_{datetime.now().timestamp()}.tex"
    with open(outpath, "w") as outfile:
        print(latex_src, file=outfile)
        latex_file_paths.append(outpath)
        print(f"Wrote {outpath}")

print("Done writing LaTeX files.")

print("Running PDFLaTeX to generate PDFs...")
for latex_file_path in latex_file_paths:
    command = ["pdflatex",
               "-interaction", "nonstopmode",
               "-output-directory", output_dir,
               latex_file_path]
    subprocess.run(command)
print(f"Done generating PDFs in {output_dir}.")
